provider "google" {
  project = "sxuereb-2fecfe97"
  region  = "us-east1"
}

data "google_project" "project" {
  project_id = "sxuereb-2fecfe97"
}

data "google_compute_network" "default" {
  name = "default"
}

data "google_compute_subnetwork" "default" {
  name   = "default"
  region = "us-east1"
}

module "gke" {
  source              = "github.com/GoogleCloudPlatform/cloud-foundation-fabric//modules/gke-cluster-autopilot?ref=v28.0.0&depth=1"
  project_id          = data.google_project.project.project_id
  name                = "routing-poc"
  deletion_protection = false
  location            = "us-east1"
  vpc_config = {
    network    = data.google_compute_network.default.self_link
    subnetwork = data.google_compute_subnetwork.default.self_link
    master_authorized_ranges = {
      internal-vms = "10.0.0.0/8"
    }
    master_ipv4_cidr_block = "192.168.0.0/28"
  }
  labels = {
    environment = "dev"
  }
  enable_features = {
    allow_net_admin = true
  }
}

# Routing layer PoC

Issue: <https://gitlab.com/gitlab-org/gitlab/-/issues/433486>

## 1. Set Up

```sh
terraform init
terraform apply
```

## 2. Connect to the cluster

Locally:

```sh
gcloud cloud-shell ssh
```

Inside of cloud-shell:

```sh
gcloud auth login
gcloud container clusters get-credentials routing-poc --region us-east1 --project sxuereb-2fecfe97
```

## 3. Install Istio

Following [istio getting started](https://istio.io/latest/docs/setup/getting-started/) inside `cloud-shell`

```sh
curl -L https://istio.io/downloadIstio | sh -
cd istio-1.20.0
export PATH=$PWD/bin:$PATH
istioctl install --set profile=demo -y
kubectl label namespace default istio-injection=enabled
```

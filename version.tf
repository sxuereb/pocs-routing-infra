terraform {
  required_providers {
    google = {
      source  = "google"
      version = "~> 5.7.0"
    }
  }
  required_version = "~> 1.6.0"
}
